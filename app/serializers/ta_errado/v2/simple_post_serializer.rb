module TaErrado
  module V2
    class SimplePostSerializer < ActiveModel::Serializer

      root false
      
      attributes :id, :category_id, :user_id, :description, :curses_count, :reports_count, :latitude, :longitude, :created_at, :image_url
  
    end
  end
end
