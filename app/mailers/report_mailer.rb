class ReportMailer < ApplicationMailer

  # report de imagens para os administradores
  def report_post(current_user, post_id)
    @post = Post.unscoped.find(post_id)
    @user = @post.user
    @current_user = current_user
    subject = "[Tá Errado App] Report Imagem -Id: #{post_id} - [taerradoapp]"

    mail(to: MainYetting.EMAIL_REPORT, subject: subject)

  end
  

  #email enviado para o dono da imagem quando for apagada
  def report_imagem_apagada(user, post_id)
    @post = Post.unscoped.find(post_id)
    @user = user
    subject = "[Tá Errado App] Imagem retirada"
    mail(to: @user.email, subject: subject)
  end
  
  #email enviado para quem fez o report da imagem
  def report_recebido(user, post_id)
    @post = Post.unscoped.find(post_id)
    @user = user
    subject = "[Tá Errado App] Imagem reportada"
    mail(to: @user.email, subject: subject)
  end
  
end