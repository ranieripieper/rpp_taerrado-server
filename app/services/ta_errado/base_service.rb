# -*- encoding : utf-8 -*-
module TaErrado
  class BaseService

    attr_reader :errors

    def valid?
      validates if @validated.blank?
      @validated = true
      return @errors.blank?
    end

    def success?
      @success == true
    end

    private
    def validates
      @errors ||= []
    end

    def add_error(error)
      @errors ||= []
      @errors << error
    end
    
    def add_errors(new_errors)
      @errors ||= []
      @errors |= new_errors
    end

    def changed?
      changed_attributes.any?
    end

    private
    def create_origin(originable, params={})
      @origin ||= originable.create_origin(origin_params(params))
    end

    def changes(old, current, attributes={})
      changes = []

      return changes if old.blank? || current.blank?

      old_attributes = old.attributes.slice(*attributes.map(&:to_s))
      new_attributes = current.attributes.slice(*attributes.map(&:to_s))


      new_attributes.each do |attribute, value|
        if old_attributes[attribute] != value
          changes << attribute
        end
      end

      changes
    end
    
  end
end


