# -*- encoding : utf-8 -*-

module API
  module V1
    class Posts < API::V1::Base

      include API::Helpers::V1::PostsHelpers

      namespace :posts do

        desc "Count posts"
        params do
          optional :date
        end
        get "/count" do
          service = TaErrado::PostCountService.new(params)
          service.count
          
          response = {
            status: 200,
            success: true,
            count: service.posts_count
          }
          response
        end
       
        desc "Retrive all new posts"
        params do
          requires :last_id #YYYYMMDDHHMMSS
        end
        get "/new" do
          authenticate_user(false)
          last_id = params[:last_id]
          
          current_user_id = -1
          if not current_user.nil? then
            current_user_id = current_user.id
          end
          
          posts = Post.select_new_post(last_id, current_user_id)
          
          response = {
            code: 200,
            success: true,
            post: post_as_json(posts)
            #count_posts: service.posts_count
          }
          
          status response[:code]

          response
              
        end
         
        desc "Retrive all posts"
        params do
          optional :page
          optional :lat
          optional :lng
          optional :date
          optional :ios
          optional :user, type: Hash do
            optional :platform
            optional :device_token
          end
        end
        get "/" do
          authenticate_user(false)
          page = params[:page].to_i || 1
          lat = params[:lat]
          lng = params[:lng]
          ios = params[:ios]
          
          if page == 1 and lat.nil? then
            service_update_token = TaErrado::UserUpdateDeviceTokenService.new(current_user, params)
            service_update_token.update
          end
          
          current_user_id = -1
          if not current_user.nil? then
            current_user_id = current_user.id
          end
          
          posts = nil
          if ios.nil? then          
            if lat.nil? or lng.nil? then
              posts = Post.select_post(page, current_user_id)
            else
              posts = Post.select_post_near(page, current_user_id, lat, lng)
            end
          else
            if lat.nil? or lng.nil? then
              posts = Post.select_post(page, current_user_id, nil, true)
            else
              posts = Post.select_post_near(page, current_user_id, lat, lng, true)
            end
          end
          
          
          service = TaErrado::PostCountService.new(params)
          service.count
          
          response = {
            code: 200,
            success: true,
            post: post_as_json(posts),
            count_posts: service.posts_count,
            android_version: 4
          }
          
          status response[:code]

          response
              
        end

        desc "delete post"
        delete "/:id" do
          authenticate_user
          post = Post.where(:id => params[:id], :user_id => current_user.id).destroy_all
          if not post.nil? and post.length > 0 then
            response = {
              status: 200,
              success: true
            }
          else
            response = {
              status: 404,
              errors: I18n.t('taerrado.errors.posts.not_found')
            }
          end
          
          response
        end
        
        desc "update post"
        params do
          requires :post, type: Hash do
            requires :description
          end
        end
        put "/:id" do
          authenticate_user
          
          service = TaErrado::PostUpdateService.new(current_user, params)
          service.update

          if service.success?
            handle_post_response(service.post)
          else
            response = {
              status: 403,
              errors: service.errors
            }
            status response[:status]

            response
          end
        end
        
        desc "get current data of post"
        get "/:id" do
          post = Post.joins(:user).includes(:user).find(params[:id]) rescue nil

          handle_post_response(post)
        end

        desc "create a new post"
        params do
          requires :post, type: Hash do
            optional :image
            requires :description
            requires :lat
            requires :lng
            requires :street
            requires :city
            requires :state
          end
        end
        post "/" do
          authenticate_user
          service = TaErrado::PostCreateService.new(current_user, params)
          service.create
          if service.success?
            response = {
              status: 200,
              success: true,
              post: post_as_json(service.post)
            }
          else
            response = {
              status: 403,
              errors: service.errors
            }
          end

          status response[:status]

          response
        end

      end
    end
  end
end
