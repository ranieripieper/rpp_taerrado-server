# -*- encoding : utf-8 -*-

module API
  module V2
    class Curses < API::V2::Base

      namespace :posts do
        desc "curse the post"
        params do
          requires :post_id
        end
        post "/:post_id/curse" do
          authenticate_user

          service = TaErrado::PostCurseCreateService.new(current_user, params)

          service.create

          if service.success?
            response = {
              success: true,
              post_curses_count: service.post_curses_count
            }
          else
            response = {
              error: true,
              errors: service.errors
            }
          end

          status service.response_status

          response
        end
        
        desc "uncurse the post"
        params do
          requires :post_id
        end
        post "/:post_id/uncurse" do
          authenticate_user

          service = TaErrado::PostCurseCreateService.new(current_user, params)

          success = service.delete

          if success == true
            response = {
              success: true,
              post_curses_count: service.post_curses_count
            }
          else
            response = {
              error: true,
              errors: service.errors
            }
          end

          status service.response_status

          response
        end
      end
    end
  end
end
