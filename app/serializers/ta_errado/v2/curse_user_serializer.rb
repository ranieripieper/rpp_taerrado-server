module TaErrado
  module V2
    class CurseUserSerializer < ActiveModel::Serializer

      attributes :user_id, :post_id
    end
  end
end
