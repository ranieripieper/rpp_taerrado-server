# -*- encoding : utf-8 -*-
module TaErrado
  class TokenAuthenticationService < AuthenticationService

    def initialize(token, options={})
      @token           = token
      @options         = options
      @response_status = 400
    end

    def authenticate
      return false unless valid?
      return false unless not @token.nil?
      @user = User.find_by( auth_token: @token )

      if @user then
          @response_status = 200
      end

      success?
    end

    def errors
      if @errors.blank? && !success?
        @response_status = 401
        add_error(I18n.t('taerrado.errors.invalid_auth_token'))
      end
      super
    end
  end
end
