class AddAddressPost < ActiveRecord::Migration
  def change
    add_column :posts, :state,    :string
    add_column :posts, :city,     :string
    add_column :posts, :street,  :string
  end
end
