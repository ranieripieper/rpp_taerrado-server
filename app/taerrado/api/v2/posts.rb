# -*- encoding : utf-8 -*-

module API
  module V2
    class Posts < API::V2::Base
      
      include API::Helpers::V2::PostsHelpers

      namespace :posts do

      desc "Teste Retrive all posts"
        params do
          optional :page
          optional :lat
          optional :lng
          optional :date
          optional :ios
          optional :user, type: Hash do
            optional :platform
            optional :device_token
          end
        end
        get "/teste" do
          authenticate_user(false)
          page = params[:page].to_i || 1
          lat = params[:lat]
          lng = params[:lng]
          ios = params[:ios]
          
          posts = Post.includes(:user).first

          #render json: ActiveModel::ArraySerializer.new(posts, {each_serializer: PostSerializer})
          
          #render json: posts
          handle_post_response(posts, {serializer: :post, root: :post})
  
        end

        desc "Count posts"
        params do
          optional :date
        end
        get "/count" do
          service = TaErrado::PostCountService.new(params)
          service.count
          
          response = {
            status: 200,
            success: true,
            count: service.posts_count
          }
          response
        end
       
        desc "Retrive all new posts"
        params do
          requires :last_id #YYYYMMDDHHMMSS
        end
        get "/new" do
          authenticate_user(false)
          last_id = params[:last_id]
          
          posts = Post.select_new_post_v2(last_id)
          
          handle_posts_response(posts, {serializer: :post, root: :posts})              
        end
         
        desc "Retrive all posts"
        params do
          optional :page
          optional :lat
          optional :lng
          optional :date
          optional :ios
          optional :user, type: Hash do
            optional :platform
            optional :device_token
          end
        end
        get "/" do
          authenticate_user(false)
          page = params[:page].to_i || 1
          lat = params[:lat]
          lng = params[:lng]
          
          if page == 1 and lat.nil? then
            service_update_token = TaErrado::UserUpdateDeviceTokenService.new(current_user, params)
            service_update_token.update
          end
         
          posts = nil
          if lat.nil? or lng.nil? then
            posts = Post.select_post_v2(page)
          else
            posts = Post.select_post_near_v2(page, lat, lng)
          end       
          
          response = handle_posts_response(posts, {serializer: :post, root: :posts})     
          
          #service = TaErrado::PostCountService.new(params)
          #service.count
          
          #response.merge!({count_posts: service.posts_count})
          response   
        end

        desc "delete post"
        delete "/:id" do
          authenticate_user
          post = Post.where(:id => params[:id], :user_id => current_user.id).destroy_all
          if not post.nil? and post.length > 0 then
            response = {
              status: 200,
              success: true
            }
          else
            response = {
              status: 404,
              errors: I18n.t('taerrado.errors.posts.not_found')
            }
          end
          
          response
        end
        
        desc "update post"
        params do
          requires :post, type: Hash do
            requires :description
          end
        end
        put "/:id" do
          authenticate_user
          
          service = TaErrado::PostUpdateService.new(current_user, params)
          service.update

          if service.success?
            handle_post_response(service.post, {serializer: :post, root: :post}) 
          else
            response = {
              status: 403,
              errors: service.errors
            }
            status response[:status]

            response
          end
        end
        
        desc "get current data of post"
        get "/:id" do
          puts "get current data of post"
          post = Post.includes(:user, :category).joins(:category,:user).find(params[:id])
          puts post.id
          handle_post_response(post, {serializer: :post, root: :post})     
        end

        desc "create a new post"
        params do
          requires :post, type: Hash do
            optional :image
            requires :description
            requires :lat
            requires :lng
            requires :street
            requires :city
            requires :state
            requires :category_id
          end
        end
        post "/" do
          authenticate_user
          service = TaErrado::PostCreateService.new(current_user, params)
          service.create
          if service.success?
            response = handle_post_response(service.post, {serializer: :post, root: :post})
          else
            response = {
              status: 403,
              errors: service.errors
            }
          end

          response
        end

      end     
    end
  end
end
