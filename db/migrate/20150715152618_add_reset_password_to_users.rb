class AddResetPasswordToUsers < ActiveRecord::Migration
  def change
    change_table(:users) do |t|
       t.string       :reset_password_token, null: true
       t.datetime     :reset_password_sended_at, null: true
       t.datetime     :password_reseted_at, null: true
    end
    
  end
end
