class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|

      t.integer   :user_id
      t.text      :description,           null: false
      t.float     :lat,                   null: false
      t.float     :lng,                   null: false
      t.integer   :curses_count,          null: false, default: 0
      t.integer   :reports_count,         null: false, default: 0
      t.timestamps null: false
    end
    
    add_attachment :posts, :image
  end
end
