# -*- encoding : utf-8 -*-

module API
  module V2
    class Categories < API::V2::Base

      namespace :categories do
        desc "Get all categories"
        get "/" do
          authenticate_user

          service = TaErrado::CategoryGetAllService.new(current_user, params)

          service.get_all

          if service.success?
            response = {
              code: 200,
              success: true,
              categories: service.categories
            }
          else
            response = {
              code: 500,
              error: true,
              errors: service.errors
            }
          end

          status response[:code]

          response
        end
        
      end
    end
  end
end
