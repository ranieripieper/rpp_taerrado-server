class CreatePostCurses < ActiveRecord::Migration
  def change
    create_table :post_curses do |t|
      t.references :post, index: true
      t.references :user, index: true

      t.timestamps null: false
    end

    add_index :post_curses, [:post_id, :user_id], unique: true
    add_foreign_key :post_curses, :posts
    add_foreign_key :post_curses, :users
  end
end
