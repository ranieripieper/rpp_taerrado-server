# -*- encoding : utf-8 -*-

require 'grape-swagger'

module API
  module V1
    class Base < API::Base

      version 'v1'
      

      include API::Helpers::V1::ApplicationHelpers
      
      mount API::V1::Users
      mount API::V1::Posts
      mount API::V1::Curses
      mount API::V1::Reports
            
      add_swagger_documentation mount_path: 'doc.json'

    end
  end
end
