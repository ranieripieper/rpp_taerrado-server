module TaErrado
  module V2
    class PostSerializer < ActiveModel::Serializer

      root false
      
      attributes :id, :category_id, :description, :curses_count, :reports_count, :latitude, :longitude, :created_at, :image_url

      has_one :user, serializer: TaErrado::V2::SimpleUserSerializer,
                                  embed: :ids,
                                  embed_in_root: true,
                                  embed_in_root_key: :linked_data
                         
      has_one :category, serializer: TaErrado::V2::CategorySerializer,
                                  embed: :ids,
                                  embed_in_root: true,
                                  embed_in_root_key: :linked_data
  
    end
  end
end
