class Category < ActiveRecord::Base

  has_many :categories, class_name: 'Category'
end
