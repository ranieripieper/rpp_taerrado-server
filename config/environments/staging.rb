Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  config.action_mailer.delivery_method = :letter_opener

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  
  config.paperclip_defaults = {
    :storage => :s3,
    :s3_host_name => "s3-us-west-2.amazonaws.com",
    :s3_credentials => {
      :bucket => "testes-ranieri", 
      :access_key_id => "AKIAJGC2JMC62K5MIHPA", 
      :secret_access_key => "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", 
    }
  }
  
  config.action_mailer.default_url_options = { :host => "http://taerradoapp.co" }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default :charset => "utf-8"
  
  config.action_mailer.smtp_settings = {
    address: "smtp.mailgun.org",
    port: 587,
    authentication: "plain",
    enable_starttls_auto: true,
    user_name: ENV["MAILGUN_USERNAME"],
    password: ENV["MAILGUN_PASSWORD"],
    domain: ENV["MAILGUN_DOMAIN"]    
      
  }
  
end
