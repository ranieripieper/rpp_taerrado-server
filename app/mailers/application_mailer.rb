class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@taerradoapp.co"
  layout 'mailer'
end
