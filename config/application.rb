# -*- encoding : utf-8 -*-
require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"

# require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TaErradoServer
  class Application < Rails::Application

    config.time_zone           = 'Brasilia'
    config.i18n.default_locale = "en".to_sym

    # load Grape API files
    config.paths.add File.join("app", "taerrado"), glob: File.join("**", "*.rb")
    config.autoload_paths += Dir.glob(File.join(Rails.root, "app", "taerrado", "{**,*}"))

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    config.assets.image_optim = false
    config.autoload_paths += %W(#{config.root}/lib)
    config.i18n.default_locale = "pt-BR"
    config.i18n.fallbacks = true
    config.i18n.enforce_available_locales = false
    config.i18n.fallbacks = ["pt-BR"]
    config.i18n.fallbacks = {'pt' => 'pt-BR'}
    config.assets.version = '1.0'
    
    # Load application ENV vars and merge with existing ENV vars. Loaded here so can use values in initializers.
    ENV.update YAML.load_file('config/application.yml')[Rails.env] rescue {}

    config.active_record.raise_in_transactional_callbacks = true
  end
end
