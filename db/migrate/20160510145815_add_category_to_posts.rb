class AddCategoryToPosts < ActiveRecord::Migration
  def change
    
    change_table(:posts) do |t|
       t.integer   :category_id
    end
    
    add_foreign_key :posts, :categories
    
  end
end