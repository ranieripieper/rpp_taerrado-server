# -*- encoding : utf-8 -*-
module API
  module Helpers
    module V1
      module ApplicationHelpers

        extend ActiveSupport::Concern

        SERIALIZERS = {
          user: TaErrado::V1::UserSerializer,
          post: TaErrado::V1::PostSerializer,
          simple_user: TaErrado::V1::SimpleUserSerializer
        }

        included do
          helpers do
          end
        end

      end
    end
  end
end
