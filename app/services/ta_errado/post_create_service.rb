# -*- encoding : utf-8 -*-
module TaErrado
  class PostCreateService < BaseService

    WHITELIST_PARAMETERS = [
      :description, :lat, :lng, :street, :city, :state, :image, :category_id
    ]

    attr_reader :user, :post, :options

    def initialize(user, post_data, options={})
      @user               = user
      @original_post_data = post_data.deep_symbolize_keys
      @post_data          = normalize_post_data(post_data)
      @options            = options
    end

    def create
      return false unless @user
      begin
        ActiveRecord::Base.transaction do
          @post = build_post
          #verifica se já existe um post em 5 minutos com a mesma descrição
          post_bd = Post.unscoped.where("description = ? and user_id = ? and created_at > ?", @post.description, @user.id, Time.now - 10.minutes).first
          if post_bd.nil? then
            if not @post.save
              add_errors(@post.errors.full_messages)
            end
          else
            @post = post_bd
          end
        end # end transaction
      rescue  => e
        add_error(e.message)
      end
    end

    def success?
      @post.try(:persisted?)
    end

    private
    def build_post
      post       = @user.posts.build(@original_post_data[:post])
      if post.category_id.nil? then
        post.category_id = 1
      end
      post
    end

    def normalize_post_data(post_data)
      post_data   = (post_data['post'] || {}).symbolize_keys
      post_data.slice(*WHITELIST_PARAMETERS).try(:to_hash).try(:symbolize_keys)
    end

  end
end
