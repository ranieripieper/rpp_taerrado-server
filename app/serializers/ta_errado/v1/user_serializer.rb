module TaErrado
  module V1
    class UserSerializer < ActiveModel::Serializer

      root :user

      has_many :posts, serializer: TaErrado::V1::PostSerializer

      attributes :id, :name, :surname, :fullname, :fb_id, :fb_token,
                 :email, :gender, :created_at, :updated_at

    end
  end
end
