# -*- encoding : utf-8 -*-
module TaErrado
  class PasswordRecoveryService < BaseService

    attr_reader :params

    RESET_PASSWORD_UPDATE_PERIOD = 30.minutes

    def initialize(params={})
      @params = params
      @user   = find_user
    end

    def request_new_password
      puts "****** #{@user}"
      if valid?
        @success = @user.reset_password!
        puts ">>>>> send mail"
        send_mail_to_user(:password_recovery)
      else
        puts ">>>>> errorrrr"
        request_new_password_error
      end
    end

    def reset_password
      if can_update_password?
        if confirmation_match?
          @success = @user.update_password(@params[:user][:password])
          
          if @success
            send_mail_to_user(:password_updated)
          else 
            add_errors(@user.errors.full_messages)
          end
        else
          add_error(I18n.t('taerrado.errors.password_combination_dont_match'))
        end
      else
        add_error(I18n.t('taerrado.errors.invalid_auth_token'))
      end
    end

    def confirmation_match?
      return false if @params[:user][:password].blank? || @params[:user][:password_confirmation].blank?
     
      return @params[:user][:password] == @params[:user][:password_confirmation]
    end

    def can_update_password?
      return false unless @params[:token]
      return false unless @user.try(:reset_password_token)

      return true
    end

    def valid?
      return false unless @user
      #return true unless @user.reset_password_sended_at

      now = Time.zone.now
      #return false if @user.reset_password_sended_at.between?((now - RESET_PASSWORD_UPDATE_PERIOD), now)

      return true
    end

    def reset_password_token
      @user.try(:reset_password_token)
    end

    def request_new_password_error
      if @user
        add_error I18n.t('taerrado.errors.cant_send_password_reset_request', remaining_time: remaining_time_for_new_request)
      else
        add_error I18n.t('taerrado.errors.user_not_found')
      end
    end

    def remaining_time_for_new_request
      remaining_seconds = ((@user.reset_password_sended_at + RESET_PASSWORD_UPDATE_PERIOD ) - Time.zone.now)
      (remaining_seconds/60).round
    end

    def find_user
      return user_from_email if @params[:user] && @params[:user][:email]
      return user_from_token if @params[:token]
    end

    def user_from_token
      User.where(reset_password_token: @params[:token]).first
    end

    def user_from_email
      User.where("email = :identifier", identifier: params[:user][:email]).first
    end

    def send_mail_to_user(action)
      mailer = UsersMailer.send(action, @user)
      mailer.deliver
    end
  end
end
