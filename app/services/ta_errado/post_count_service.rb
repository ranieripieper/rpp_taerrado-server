# -*- encoding : utf-8 -*-
module TaErrado
  class PostCountService < BaseService

    def initialize(params, options={})
      @params             = params
      @options            = options
    end

    def count
      date = @params[:date]
      if date.nil?  then
        @count_posts = Post.count
      else
        date = DateTime.parse(date).to_date
        date_prev = date - 1.day
        @count_posts = Post.where(["created_at >= ? AND created_at <= ?", date.beginning_of_day, date.end_of_day]).count
      end
    end
    
    def posts_count
      @count_posts
    end

  end
end
