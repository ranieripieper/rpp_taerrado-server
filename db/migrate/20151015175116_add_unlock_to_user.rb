class AddUnlockToUser < ActiveRecord::Migration
  def change
    add_column :users, :unlock, :boolean, default: false
  end
end
