class User < ActiveRecord::Base
  
  acts_as_paranoid
  
  has_secure_password :validations => false
  
  has_many :curses, class_name: 'PostCurse', dependent: :destroy
    
  #mount_uploader :profile_image, ProfileImageUploader
  
  has_attached_file :profile_image, 
                 :content_type => /\Aimage\/.*\Z/,
                 processors: [:thumbnail, :paperclip_optimizer], 
                 :default_url => :set_default_profile_image_url
                 
  validates_attachment_content_type :profile_image, :content_type => ["image/jpg", "image/jpeg", "image/png"]

  alias_attribute :profile_img, :profile_image

  EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: EMAIL_REGEXP }, if: -> { self.email.present? }

  validates :name, presence: true

  #validates :fb_id, presence: true
  #validates :fb_token, presence: true
  validates :password, :password_confirmation, :length => { :minimum => 5 }, if: -> {
    (self.fb_id.nil? or self.fb_id.blank? ) and (self.new_record? || self.password_digest_changed?)
  }

  validates :password, confirmation: true, if: -> {
    (self.fb_id.nil? or self.fb_id.blank? ) and (self.new_record? || self.password_digest_changed?)
  }
  
  has_many :posts, class_name: 'Post', :dependent => :destroy
  has_many :curses, class_name: 'PostCurse'
  has_many :reports, class_name: 'PostReport'
  
  before_save :ensure_authentication_token

  def as_json(options = { })
    h = super(options)
    h[:profile_image] = profile_image_url
    h
  end
  
  def cursed_post?(post_id)
    self.curses.where(post_id: post_id).exists?
  end
  
  def set_default_profile_image_url
    if self.fb_id.nil? or self.fb_id.blank? then
      "http://taerradoapp.co/img/img_placeholder_user.png"
    else
      "http://graph.facebook.com/#{self.fb_id}/picture?type=large"
    end
  end
  
  def self.authenticate(email, password)
    return false unless email && password
    user = User.find_by(email: email)

    return false unless user
    return false unless user.email
    return false unless user.password_digest
    user.try(:authenticate, password)
  end

  def profile_image_url
    if profile_img.url.nil? or profile_img.url.blank? then
      URI.unescape("http://taerradoapp.co/img/img_placeholder_user.png")
    else
      URI.unescape(profile_img.url)
    end
  end
  
  def fullname
    [self.name, self.surname].join(' ')
  end
  
  def curses_count
    self.curses.count
  end

  def posts_count
    self.posts.count
  end
  
  def reset_password!
    self.reset_password_token     = [self.id, SecureRandom.hex].join
    self.reset_password_sended_at = Time.now
    self.save!
  end
  
  def update_password(new_password)
    self.password = self.password_confirmation = new_password
    self.password_reseted_at  = Time.now
    self.reset_password_token = nil
    self.save(:validate => false)
  end
  
  def self.validate_password
    self.fb_id.nil?
  end
  
  private
  def ensure_authentication_token
    if auth_token.blank?
      self.auth_token = generate_authentication_token
    end
  end
  
  def generate_authentication_token
    loop do
      token = [self.id, SecureRandom.hex, Time.now.to_i].join #SecureRandom.urlsafe_base64(nil, false)
      break token unless User.where(auth_token: token).first
    end
  end
end
