# -*- encoding : utf-8 -*-
module API
  module Helpers
    module V1
      module PostsHelpers

        extend ActiveSupport::Concern

        included do

          helpers do

            def handle_post_response(post, serializer=:post)
              response = {
                code: 200,
                success: true,
                post: post_as_json(post)
              }
              
              status response[:code]

              response
            end

            def post_as_json(post, load_user=true)
              options = {:methods => [:distance, :user_curse, :image_url], 
                              :except => [:image_file_name, :image_file_size, :image_content_type, :image_updated_at, :user_id]}
              if load_user then
                options = options.deep_merge(:include => {
                                :user => {                                  
                                  :only => [:id, :fb_id],
                                  :methods => [:profile_image_url]
                                }
                              })
               end
               
               post.as_json(options)
            end
          end
        end
      end
    end
  end
end
