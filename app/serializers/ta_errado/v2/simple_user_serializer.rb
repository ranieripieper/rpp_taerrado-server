module TaErrado
  module V2
    class SimpleUserSerializer < ActiveModel::Serializer
      root 'user'
      
      attributes :id, :name, :surname, :fullname
      
      puts ">>>> SimpleUserSerializer"
    end
  end
end
