# -*- encoding : utf-8 -*-
module API
  module Helpers
    module ApplicationHelpers

      extend ActiveSupport::Concern

      VALID_LOCALES = [
        :"pt-BR"
      ]

      included do
        helpers do

          def get_serializer(serializer=nil)
            get_serializer_by_key_name(serializer).constantize
          end

          def get_serializer_by_key_name(serializer_name)
            puts serializer_name
            version = env['grape.routing_args'][:version]
            "ta_errado/#{version}/#{serializer_name.to_s}_serializer".camelize
          end
          
          def serialized_object(object, options)
            serializer = get_serializer(options.delete(:serializer))
            serializer.new(object, options)
          end

          def serialized_array(collection, options={})
            serializer = get_serializer(options.delete(:serializer))
            options    = options.merge(each_serializer: serializer, scope: current_user)

            ActiveModel::ArraySerializer.new(collection, options)
          end

          def in_sandbox_environment?
            ["development", "staging"].member?(Rails.env.to_s)
          end

          def set_locale
            I18n.locale = get_current_locale
          end

          #def authenticate_user
          #  unless current_user
          #    response = token_authentication_error_response
          #    error!( response, response[:status] )
          #  end
          #end
          
          def authenticate_user(response_error = true)
            unless current_user
              if response_error
                response = token_authentication_error_response
                error!( response, response[:status] )
              end
            end
          end

          def current_user
            return @current_user if @current_user
            token_authentication_service = get_token_authentication_service
            token_authentication_service.authenticate
            @current_user = token_authentication_service.try(:user)
          end


          def get_authentication_service
            @authentication_service ||= TaErrado::AuthenticationService.new( params )
          end
          
          def get_authentication_token
            token = params['X-Token'] || headers['X-Token'] || params['auth_token'] || headers['auth_token']
            token
          end

          def get_current_locale
            locale = (params[:locale] || headers['X-Locale'])
            locale.presence && VALID_LOCALES.member?(locale.to_sym) ? locale : I18n.default_locale
          end

          def get_current_ip
            env['HTTP_X_FORWARDED_FOR'] || env['REMOTE_ADDR']
          end

          def get_origin_object
            @origin ||= {
              ip: get_current_ip,
              user_agent: env['HTTP_USER_AGENT'],
              locale: I18n.locale
            }
          end

          def get_token_authentication_service
            @token_authentication_service ||= TaErrado::TokenAuthenticationService.new( get_authentication_token )
          end

          def token_authentication_error_response
            {
              error: true,
              status: 401,
              errors: get_token_authentication_service.errors
            }
          end

        end
      end
    end
  end
end
