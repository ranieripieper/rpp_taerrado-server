# -*- encoding : utf-8 -*-
module TaErrado
  class PostUpdateService < BaseService

    WHITELIST_PARAMETERS = [
      :id, :description
    ]

    attr_reader :user, :post, :options

    def initialize(user, post_data, options={})
      @user               = user
      @original_post_data = post_data.deep_symbolize_keys
      @post_data          = normalize_post_data(post_data)
      @options            = options
      @success            = false
    end

    def update
      return false unless @user

      @post = find_post_by_id_and_user
      if @post.nil? then
        @response_status = 404
        add_error(I18n.t('taerrado.errors.posts.not_found'))
      else
        if not @post.update_attributes(@post_data)
          add_errors(@post.errors.messages)
        else
          @success = true
        end
      end
    end

    def success?
      @success
    end

    private
    
    def find_post_by_id_and_user
      Post.where(:id => @original_post_data[:id], :user_id => @user.id).first
    end

    def normalize_post_data(post_data)
      post_data   = (post_data['post'] || {}).symbolize_keys
      post_data.slice(*WHITELIST_PARAMETERS).try(:to_hash).try(:symbolize_keys)
    end

  end
end
