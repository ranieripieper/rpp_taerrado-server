# -*- encoding : utf-8 -*-

require 'grape-swagger'

module API
  module V2
    class Base < API::Base

      version 'v2'
      
      format :json
      formatter :json, Grape::Formatter::ActiveModelSerializers
    
      include API::Helpers::V2::ApplicationHelpers
      
      mount API::V2::Users
      mount API::V2::Posts
      mount API::V2::Curses
      mount API::V2::Reports
      mount API::V2::Categories

      add_swagger_documentation mount_path: 'doc.json'

    end
  end
end
