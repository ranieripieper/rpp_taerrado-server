# -*- encoding : utf-8 -*-
module TaErrado
  class UserGetPostsService

    attr_reader :params, :errors

    WHITELIST_PARAMETERS = [
      :page,
      :id
    ]

    def initialize(current_user, params={})
      @current_user = current_user
      @original_params = params
      @params          = normalize_params(params)
      @errors          = []
    end

    def get(force_user_id = false, ios = false)
      
      page = @params[:page].to_i || 1
      if page == 0 then
        page = 1
      end
      user_id = @params[:id]
      if force_user_id and user_id.nil? then
        user_id = @current_user.id
      end
      current_user_id = -1
      if not @current_user.nil? then
        current_user_id = @current_user.id
      end
      @posts = Post.select_post(page, current_user_id, user_id, ios)
      @user = nil
      
      if page == 1 then
        @user = User.select(:id, :fb_id, :profile_image_updated_at, :profile_image_file_name, :created_at).find(user_id)
      end
      
    end

    def user
      @user
    end

    def posts
      @posts
    end

    private

    def normalize_params(params)
      params            = (params || {}).deep_symbolize_keys
      params.slice(*WHITELIST_PARAMETERS).try(:to_hash).try(:symbolize_keys)
    end
  end
end
