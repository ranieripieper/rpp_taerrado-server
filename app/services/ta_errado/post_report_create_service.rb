# -*- encoding : utf-8 -*-
module TaErrado
  class PostReportCreateService < BaseService

    attr_reader :user, :post, :report, :options, :response_status

    def initialize(user, options={})
      @user            = user
      @options         = options.symbolize_keys
      @response_status = 201
    end

    def create
      @post = find_post rescue nil

      if valid_post?
        begin
          @report = build_report
          @report.save
          @post.reports_count = @post.reports.count
          @post.save
          
          #if admin, apaga o post
          if @user.admin then
            @post.delete
          end
          
          #send mails
          Thread.new do
            if @user.admin then
              ReportMailer.report_imagem_apagada(@post.user, @post.id).deliver_later
            else
              ReportMailer.report_recebido(@user, @post.id).deliver_later
            end
            ReportMailer.report_post(@user, @post.id).deliver_later
          end
        rescue ActiveRecord::RecordNotUnique => e
          @response_status = 403
          add_error(I18n.t('taerrado.errors.posts.reports.user_cant_like_post_twice'))
        end
      else
        @response_status = 404
        add_error(I18n.t('taerrado.errors.posts.not_found'))
      end
    end

    def success?
      @report.try(:persisted?)
    end

    def valid_post?
      @post && @post.is_a?(Post)
    end

    def post_reports_count
      @post_reports_count ||= @post.nil? ? nil : @post.reports_count
    end
    
    def post
      puts ">>>>>> #{@post.id}"
      @post
    end

    private
    def find_post
      Post.find(@options[:post_id])
    end
    
    def build_report
      @post.reports.build(user_id: @user.id)
    end

  end
end
