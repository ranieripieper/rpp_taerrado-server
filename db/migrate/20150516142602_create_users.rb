class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.string :gender
      t.string :fb_id
      t.text   :fb_token
      t.string :auth_token

      t.timestamps null: false
    end
  end
end
