# -*- encoding : utf-8 -*-
module TaErrado
  class UserDeleteService < BaseService

    attr_reader :user, :errors, :response_status

    def initialize(user, options={})
      @user               = user
      @options            = options
      @response_status    = 400
    end

    
    def execute
      if valid_user? then
        if @user.destroy then
          @response_status    = 200
        else
           add_error(I18n.t('taerrado.errors.users.cant_delete'))
        end
      end
    end

    def success?
      @response_status == 200
    end

    def user_params
      params = (@options[:user] || {}).slice(*WHITELIST_PARAMETERS)

      params
    end

    def valid_user?
      @user && @user.is_a?(User)
    end

  end
end
