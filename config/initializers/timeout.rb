if ENV['RAILS_ENV'] != 'development' then
  Rack::Timeout.timeout = Integer(ENV['TIMEOUT'] || MainYetting.TIMEOUT)  # seconds
  
  
  if Rack::Utils.respond_to?("key_space_limit=")
    Rack::Utils.key_space_limit = 262144
  end
end