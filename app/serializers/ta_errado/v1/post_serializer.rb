module TaErrado
  module V1
    class PostSerializer < ActiveModel::Serializer
      
      root false

      attributes :id, :user_id, :description, :curse_count, :report_count, :latitude, :longitude, :created_at

     has_one :user, serializer: TaErrado::V1::SimpleUserSerializer
     puts ">>>> PostSerializer"
    end
  end
end
