# -*- encoding : utf-8 -*-
module API
  module V2
    class Reports < API::V2::Base

      namespace :posts do
        desc "report the post"
        params do
          requires :post_id
        end
        post "/:post_id/report" do
          authenticate_user

          params[:origin] = get_origin_object

          service = TaErrado::PostReportCreateService.new(current_user, params)

          service.create

          if service.success?
            response = {
              success: true,
              post_reports_count: service.post_reports_count
            }
          else
            response = {
              error: true,
              errors: service.errors
            }
          end

          status service.response_status

          response
        end

      end
    end
  end
end
