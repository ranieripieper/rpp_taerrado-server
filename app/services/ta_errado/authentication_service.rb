# -*- encoding : utf-8 -*-
module TaErrado
  class AuthenticationService < BaseService

    attr_reader :params, :response_status, :user

    USER_WHITELIST_PARAMETERS = [
      :email,
      :password,
      :platform,
      :device_token,
      :options
    ]

    def initialize(params, options={})
      @original_params = params
      @params          = normalize_params(params)
      @response_status = 401
    end

    def authenticate

      return false unless valid?
      return false unless not @params[:user][:email].nil?
      return false unless not @params[:user][:password].nil? || @params[:user][:password].blank?
      
      @user = User.authenticate(@params[:user][:email], @params[:user][:password])

      if authenticated?
        service_update_token = TaErrado::UserUpdateDeviceTokenService.new(@user, @original_params)
        service_update_token.update
        @response_status = 200
      else
        add_error(I18n.t('hipapp.errors.invalid_user_credentials'))
      end

      success?
    end

    def authenticated?
      @user.present?
    end

    def success?
      authenticated?
    end

    def user_auth_token
      @auth_token.try(:token)
    end
    
    def normalize_params(params={})
      params            = (params || {}).deep_symbolize_keys
      normalized_params = {}
      user_params       = params[:user]

      normalized_params[:user] = user_params.slice(*USER_WHITELIST_PARAMETERS)

      normalized_params
    end
    
  end
end
