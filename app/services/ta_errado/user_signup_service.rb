# -*- encoding : utf-8 -*-
module TaErrado
  class UserSignupService < BaseService

    attr_reader :params, :errors, :user

    USER_WHITELIST_PARAMETERS = [
      :name,
      :surname,
      :email,
      :gender,
      :fb_id,
      :fb_token,
      :platform,
      :device_token,
      :password,
      :password_confirmation,
      :profile_image
    ]

    def initialize(params={})
      @original_params = params
      @params          = normalize_params(params)
      @errors          = []
    end

    def register
      @user = get_user
      
      if (@user.id.nil? and not @user.save) or 
         (not @user.id.nil? and not @user.update_attributes(@params[:user])) then
         add_errors(@user.errors.full_messages)
      end
    end

    def success?
      @user.try(:persisted?)
    end


    def id
      @user.try(:id)
    end

    def user_email
      @user.try(:email)
    end

    def user_fb_id
      @user.try(:fb_id)
    end
    
    def user_auth_token
      @user.try(:auth_token)
    end

    private

    def get_user
      #verifica se usuário já existe
      user = nil
      #verifica primeiro pelo fb_id
      if not @params[:user][:fb_id].nil? then
        user = User.find_by_fb_id(@params[:user][:fb_id])
      end
      
      if user.nil? then
        user = User.new(@params[:user])
      end

      user
    end
    
    def normalize_params(params={})
      params            = (params || {}).deep_symbolize_keys
      normalized_params = {}
      user_params       = params[:user]

      normalized_params[:user] = user_params.slice(*USER_WHITELIST_PARAMETERS)

      normalized_params
    end

  end
end
