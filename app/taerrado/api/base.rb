# -*- encoding : utf-8 -*-

module API
  class Base < Grape::API

    before do
      @log_start_t = Time.now
      Rails.logger.info "  Parameters: #{params.to_hash.except("route_info")}"
    end
    
    after do
      @log_end_t = Time.now
      total_runtime = ((@log_end_t -  @log_start_t) * 1000).round(1)
      db_runtime = (ActiveRecord::RuntimeRegistry.sql_runtime || 0).round(1)
      Rails.logger.info "Completed in #{total_runtime}ms (ActiveRecord: #{db_runtime}ms)"
    end
  
    prefix 'api'

    format :json
    formatter :json, Grape::Formatter::ActiveModelSerializers

    include Grape::Kaminari

    include API::Helpers::ApplicationHelpers

    before do
      set_locale
      get_origin_object
    end

    mount API::V1::Base
    mount API::V2::Base
  end
end
