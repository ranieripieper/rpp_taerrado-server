# -*- encoding : utf-8 -*-
module API
  module Helpers
    module V2
      module PostsHelpers

        extend ActiveSupport::Concern

        included do

          helpers do

            def handle_posts_response(posts, options)
              curses = []
              if not current_user.nil? and not posts.nil? then
                curses = PostCurse.where("user_id = ? and post_id in (?) ", current_user.id, posts.map{|h| h.id }).all
              end 
              
              response = {
                code: 200,
                success: true 
              }
              
              response.merge!(posts_as_json(posts, options).as_json) 
              
              if not curses.nil? then
                response[:linked_data].merge!(user_curses_as_json(curses).as_json)
              end
              
              status response[:code]
              
              response
            end
            
            def handle_post_response(post, options)
              curses = []
              if not current_user.nil? and not post.nil? then
                curses = PostCurse.where("user_id = ? and post_id = ? ", current_user.id, post.id).all
              end
              response = {
                code: 200,
                success: true 
              }
              
              response.merge!(post_as_json(post, options).as_json) 
              if not curses.nil? and not curses.empty? then
                if response[:linked_data].nil? then
                  response[:linked_data] = user_curses_as_json(curses).as_json
                else
                  response[:linked_data].merge!(user_curses_as_json(curses).as_json)
                end
              end
              status response[:code]
              
              response

            end

            def post_as_json(post, options={})
              puts "xxxxxxxxxxxx"
              serialized_object(post, options)
            end
            
            def posts_as_json(posts, options={})
               puts "xxxxxxxxxxxx posts_as_json"
              serialized_array(posts, options)
            end
            
            def user_curses_as_json(curses, options={serializer: :curse_user, root: "user_cursed"})
              serialized_array(curses, options)
            end
            
          end
        end
      end
    end
  end
end
