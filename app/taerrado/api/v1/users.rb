# -*- encoding : utf-8 -*-

module API
  module V1
    class Users < API::V1::Base

      include API::Helpers::V1::UsersHelpers
      include API::Helpers::V1::PostsHelpers

      namespace :users do
        
        desc "Retrive all posts"
        params do
          optional :page
        end
        get "/posts" do
          authenticate_user
          
          ios = params[:ios]
          is_ios = !ios.nil?
          service = TaErrado::UserGetPostsService.new(current_user, params)
          service.get(true, !ios.nil?)

          response = {
            success: true,
            user: user_as_json(service.user, true),
            posts: post_as_json(service.posts, false)
          }

          response
        end
        
        desc "Get user info"
        get "/:id" do          
          response = {
            success: true,
            user: User.select(:id, :profile_image_updated_at, :profile_image_file_name, :created_at).find(params[:id].to_i)
          }

          response
        end
        
        desc "Retrive all posts by user"
        params do
          optional :page
        end
        get "/:id/posts" do
          authenticate_user(false)
          service = TaErrado::UserGetPostsService.new(current_user, params)
          service.get

          response = {
            success: true,
            user: user_as_json(service.user, true),
            posts: post_as_json(service.posts, false)
            
          }

          response
        end
        
        desc "Create a new user"
        #curl -F "user[email]=teste@teste.com" -F "user[fb_id]=123" -F "user[fb_token]=1234567" -F "user[gender]=male" -F "user[surname]=xxx" -F "user[name]=xxx" https://ta-errado-stg.herokuapp.com/api/v1/users
        params do
          requires :user, type: Hash do
            requires :name
            optional :surname
            requires :email
            optional :gender
            optional :fb_id
            optional :fb_token
            optional :password
            optional :platform
            optional :device_token
            optional :password_confirmation
            optional :profile_image
          end
        end
        
        post '/' do
          
          params[:origin] = get_origin_object

          signup_service  = TaErrado::UserSignupService.new(params)
          signup_service.register

          if signup_service.success?
            response = user_success_response_for_service(signup_service)
          else
            status 400
            response = {
              error: true,
              code: 400,
              errors: signup_service.errors
            }
          end

          response
        end

        desc "Authenticate a user using email and password"
        params do
          requires :user, type: Hash do
            requires :email   , type: String
            requires :password, type: String
            optional :platform, type: String
            optional :device_token, type: String
          end
        end

        post '/auth' do
          authentication_service = get_authentication_service
          authentication_service.authenticate

          if authentication_service.success?
            response = user_success_response_for_service(authentication_service)
          else
            response_status = authentication_service.response_status
            status response_status

            response = {
              error: true,
              success: false,
              code: response_status,
              errors: authentication_service.errors
            }
          end

          response

        end
        
        desc "Send a password reset email to user"
        params do
          requires :user, type: Hash do
            requires :email
          end
        end
        post '/password_reset' do
          service = TaErrado::PasswordRecoveryService.new(params)
          service.request_new_password

          if service.success?
            status 201
            response = {
              success: true
            }
          else
            status 400

            response = {
              error: true,
              code: 400,
              errors: service.errors
            }
          end
        end

        desc "Update user password"
        params do
          requires :user, type: Hash do
            requires :password
            requires :password_confirmation
          end
        end
        put '/password_reset/:token' do
          service  = TaErrado::PasswordRecoveryService.new(params)
          response = service.reset_password

          if service.success?
            response = {
              success: true
            }
          else
            status 400
            response = {
              success: false,
              error: true,
              code: 400,
              errors: service.errors
            }
          end

          response
        end

        desc "Edit current_user profile"
        put "/" do
          authenticate_user

          service = TaErrado::UserUpdateService.new(current_user, params)
          service.execute

          status service.response_status

          if service.success?
            response = {
              success: true,
              user: user_as_json(current_user.reload)
            }
          else
            response = {
              success: false,
              error: true,
              errors: service.errors
            }
          end
        end

        desc "Delete user profile"
        delete "/" do
          authenticate_user

          service = TaErrado::UserDeleteService.new(current_user)
          service.execute

          status service.response_status

          if service.success?
            response = {
              success: true
            }
          else
            response = {
              success: false,
              error: true,
              errors: service.errors
            }
          end
        end 
                 
      end
    end
  end
end
