class CreatePostReports < ActiveRecord::Migration
  def change
    create_table :post_reports do |t|

      t.integer :post_id, index: true
      t.integer :user_id, index: true

      t.timestamps null: false
    end

    add_index :post_reports, [:post_id, :user_id], unique: true
  end
end
