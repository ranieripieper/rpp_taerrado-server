class AddPasswordProfileToUsers < ActiveRecord::Migration
  def change
    
    change_table(:users) do |t|
       t.string       :password_digest, null: true
    end
    
    add_attachment :users, :profile_image
    
  end
end
