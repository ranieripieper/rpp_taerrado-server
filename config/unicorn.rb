

if not ENV['AZURE'].nil? and ENV['AZURE'] == 'true' then
  # Set the working application directory
  # working_directory "/path/to/your/app"
  working_directory "/var/www/ta-errado-server"
  
  # Unicorn PID file location
  # pid "/path/to/pids/unicorn.pid"
  pid "/var/www/pids/ta-errado-server_unicorn.pid"
  
  # Path to logs
  # stderr_path "/path/to/log/unicorn.log"
  # stdout_path "/path/to/log/unicorn.log"
  stderr_path "/var/www/ta-errado-server/log/unicorn.log"
  stdout_path "/var/www/ta-errado-server/log/unicorn.log"
  
  # Unicorn socket
  listen 8081

else
  # Set the working application directory
  # working_directory "/path/to/your/app"
  working_directory "./"
  
  # Unicorn PID file location
  # pid "/path/to/pids/unicorn.pid"
  pid "log/ta-errado-server_unicorn.pid"
  
  # Path to logs
  # stderr_path "/path/to/log/unicorn.log"
  # stdout_path "/path/to/log/unicorn.log"
  stderr_path "log/unicorn.log"
  stdout_path "log/unicorn.log"
  
  # Unicorn socket
  listen 3000
end

#listen "/var/www/socket/bemychef_unicorn.sock"

# Number of processes
# worker_processes 4
worker_processes Integer(ENV['WEB_CONCURRENCY'] || 5)
timeout Integer(ENV['UNICORN_TIMEOUT'] || 29) + 1
preload_app true


before_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn master intercepting TERM and sending myself QUIT instead'
    Process.kill 'QUIT', Process.pid
  end

  if defined?(ActiveRecord::Base)
    ActiveRecord::Base.connection.disconnect!
    puts '>>>> Disconnected from ActiveRecord'
    Rails.logger.info('Disconnected from ActiveRecord')
  end

end

after_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to send QUIT'
  end

  if defined?(ActiveRecord::Base)
    config = ActiveRecord::Base.configurations[Rails.env] ||
                Rails.application.config.database_configuration[Rails.env]
    config['reaping_frequency'] = ENV['DB_REAP_FREQ'] || 10 # seconds
    config['pool']            =   ENV['DB_POOL'] || 3
    ActiveRecord::Base.establish_connection(config)
    Rails.logger.info('Connected to ActiveRecord')
  end

end