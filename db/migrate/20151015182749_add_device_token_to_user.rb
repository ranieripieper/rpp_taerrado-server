class AddDeviceTokenToUser < ActiveRecord::Migration
  def change
    add_column :users, :platform,         :string
    add_column :users, :device_token,     :string
  end
end
