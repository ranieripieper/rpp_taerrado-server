# -*- encoding : utf-8 -*-
module TaErrado
  class PostCurseCreateService < BaseService

    attr_reader :user, :post, :curse, :options, :response_status

    def initialize(user, options={})
      @user            = user
      @options         = options.symbolize_keys
      @response_status = 201
    end

    def create
      @post = find_post rescue nil

      if valid_post?
        begin
          @curse = PostCurse.unscoped.where(:post_id => @options[:post_id], :user_id => @user.id).first
          if @curse.nil? then
            @curse = build_curse
            @curse.save
            notify_curse(@user, @post)
          else
            if @curse.deleted_at? then
              @curse.restore
            end
          end
          @post.curses_count = @post.curses.count
          @post.save
        rescue ActiveRecord::RecordNotUnique => e
          @response_status = 403
          add_error(I18n.t('taerrado.errors.posts.curses.user_cant_like_post_twice'))
        end
      else
        @response_status = 404
        add_error(I18n.t('taerrado.errors.posts.not_found'))
      end
    end
    
    def delete

      @curse = find_curse_by_post_user rescue nil
      
      exec_delete = PostCurse.where(:post_id => @options[:post_id], :user_id => @user.id)
      if not exec_delete.nil? then
        exec_delete.each do |post_curse|
          post_curse.delete
        end
      end
      @post = find_post
      @post.curses_count = @post.curses.count
      @post.save
=begin          
      if exec_delete > 0 then
        @post = find_post rescue nil
        return true
      else
        @response_status = 404
        add_error(message: I18n.t('taerrado.errors.posts.not_found'))
      end
=end      
      return true
    end

    def success?
      @curse.try(:persisted?)
    end

    def valid_post?
      @post && @post.is_a?(Post)
    end

    def post_curses_count
      @post_curses_count ||= @post.nil? ? nil : @post.curses_count
    end

    private
    def find_post
      Post.find(@options[:post_id])
    end
    
    def build_curse
      @post.curses.build(user_id: @user.id)
    end

    def notify_curse(user, post)
      require 'parse-ruby-client'
      if not post.user.device_token.nil? and not post.user.device_token.blank? then
        if user.id == post.user.id then
          return
        end
        client = Parse.create(
          :application_id => MainYetting.PARSE_APP_ID, 
          :api_key        => MainYetting.PARSE_API_KEY, 
          :quiet      => false)
    
        msg = "Alguém xingou sua foto."
        post_id = post.id
  
        data = { :alert => msg, :post_id => post_id, :post_image => post.image_url }
        push = client.push(data)
        query = client.query(Parse::Protocol::CLASS_INSTALLATION).eq('deviceToken', post.user.device_token)
        push.type = post.user.platform
        push.where = query.where    
        push.save
      end
    end
  
  end
  

  
end
