# -*- encoding : utf-8 -*-
module API
  module Helpers
    module V1
      module UsersHelpers

        extend ActiveSupport::Concern

        included do
          helpers do

            def handle_user_post_response(user, posts, serializer=:post)
              response = {
                code: 200,
                success: true,
                post: post_as_json(post, serializer)
              }
              
              status response[:code]

              response
            end
            
            def user_success_response_for_service(service)

              status service.response_status if service.respond_to?(:response_status)

              response = {
                success: true,
                user_data: user_as_json(service.user)
              }
            end
            
            def user_as_json(user, load_couns=false)
              options = { }
              if load_couns then
                options = options.deep_merge(:methods => [:curses_count, :posts_count, :profile_image_url])
              else
                options = options.deep_merge(:methods => [:profile_image_url])
              end
              options = options.deep_merge(:except => [:reset_password_token, :reset_password_sended_at, :password_reseted_at, :password_digest, :profile_image_file_name, :profile_image_content_type, :profile_image_file_size, :profile_image_updated_at])
              user.as_json(options)
            end

          end
        end

      end
    end
  end
end
