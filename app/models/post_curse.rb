class PostCurse < ActiveRecord::Base
  
  acts_as_paranoid
  
  belongs_to :user

  belongs_to :post

  validates :user_id, :post_id, presence: true

end
