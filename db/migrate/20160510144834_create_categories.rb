class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.text      :name,           null: false
      t.text      :description,    null: true
      t.text      :icon,           null: false
    end
  end
end
