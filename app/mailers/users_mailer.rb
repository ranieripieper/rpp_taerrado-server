class UsersMailer < ApplicationMailer
  
  def password_recovery(user)
    @user = user

    mail(to: user.email, subject: '[Tá Errado App] Resgate de Senha')
  end

  def password_updated(user)
    @user = user

    mail(to: user.email, subject: '[Tá Errado App] Senha alterada')
  end
  
end
