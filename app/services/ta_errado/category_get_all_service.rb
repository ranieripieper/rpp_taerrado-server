# -*- encoding : utf-8 -*-
module TaErrado
  class CategoryGetAllService

    attr_reader :params, :errors

    def initialize(current_user, params={})
      @current_user   = current_user
      @original_params = params
      @params          = normalize_params(params)
      @errors          = []
      @success         = false
    end

    def get_all()
      @categories = Category.order(:id)
      @success = true;
    end

    def categories
      @categories
    end

    def success?
      @success
    end
    
    private

    def normalize_params(params)
      params            = (params || {}).deep_symbolize_keys
    end
  end
end
