class AddDeletedAtToCurse < ActiveRecord::Migration
  def change
    add_column :post_curses, :deleted_at, :datetime
    add_index :post_curses, :deleted_at
  end
end
