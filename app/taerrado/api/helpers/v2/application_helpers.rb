# -*- encoding : utf-8 -*-
module API
  module Helpers
    module V2
      module ApplicationHelpers

        extend ActiveSupport::Concern
        
        SERIALIZERS_V2 = {
          user: TaErrado::V2::UserSerializer,
          post: TaErrado::V2::PostSerializer,
          category: TaErrado::V2::CategorySerializer,
          curse_user: TaErrado::V2::CurseUserSerializer,
          simple_user: TaErrado::V2::SimpleUserSerializer,
          simple_post: TaErrado::V2::SimplePostSerializer
        }

        included do
          helpers do
          end
        end

      end
    end
  end
end
