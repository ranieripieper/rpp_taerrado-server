# -*- encoding : utf-8 -*-
module TaErrado
  class UserUpdateDeviceTokenService < BaseService
    
    WHITELIST_PARAMETERS = [
      :device_token, :platform
    ]
    
    def initialize(user, params, options={})
      @user               = user
      @user_data          = normalize_user_data(params)
      @params             = params
      @options            = options
    end

    def update
      if not @user.nil? and not @params[:user].nil? and not @params[:user][:device_token].nil? and not @params[:user][:platform].nil? then
        @user.update_attributes(@user_data)
      end
    end
    
    private
    def normalize_user_data(user_data)
      user_data   = (user_data['user'] || {}).symbolize_keys
      user_data.slice(*WHITELIST_PARAMETERS).try(:to_hash).try(:symbolize_keys)
    end
    
  end
end
