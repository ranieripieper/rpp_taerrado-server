class Post < ActiveRecord::Base

  acts_as_paranoid

  DISTANCE_FIND = 3000
  self.per_page = 30
  
  belongs_to :user, class_name: 'User'
  belongs_to :category

  has_many :curses, class_name: 'PostCurse', :dependent => :destroy
  has_many :reports, class_name: 'PostReport', :dependent => :destroy

  has_attached_file :image,
                 :content_type => /\Aimage\/.*\Z/,
                 processors: [:thumbnail, :paperclip_optimizer], 
                 :default_url => "http://placehold.it/300x300"
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png"]
  
  validates :user_id, :description, presence: true

  attr_accessor :user_curse

  alias_attribute :latitude, :lat
  alias_attribute :longitude, :lng
  
  def self.select_new_post_v2(last_id)
    Post.where("posts.id > ? ", last_id).includes(:user, :category).order(created_at: :asc)
  end
  
  
  def self.select_post_v2(page)
    if page == 0 then
      page = 1
    end
    
    Post
        .includes(:user, :category)
        .order(created_at: :desc)
        .paginate(:page => page, :total_entries => 1)
    
    #, :total_entries => 1 prevent call count https://github.com/mislav/will_paginate/issues/316
  end
  
  def self.select_post_near_v2(page, lat, lng)
    if page == 0 then
      page = 1
    end

    Post
        .select("#{Post.table_name}.*, (
                        3959 * acos (cos ( radians(#{lat}) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(#{lng}) )
                        + sin ( radians(#{lat}) )
                        * sin( radians( lat ) )
                      )) AS distance")
        .includes(:user, :category)
        .order(created_at: :desc)
        .having("distance < ?", DISTANCE_FIND)
        .paginate(:page => page, :total_entries => 1)

    #, :total_entries => 1 prevent call count https://github.com/mislav/will_paginate/issues/316

  end
  
  def self.select_new_post(last_id, curse_user_id, user_id = nil)

    if user_id.nil? then
      posts_array = Post.joins(:user)
                  .includes(:user)
                  .where("posts.id > ? ", last_id)
                  .joins("LEFT OUTER JOIN `post_curses` ON `post_curses`.`post_id` = `posts`.`id` and `post_curses`.`user_id` = #{curse_user_id}")
                  .order(created_at: :asc)
                  .paginate(:page => 1)
                  .pluck_to_hash(:id, :category_id, :curses_count, :street, :city, :state, :reports_count, :description, :created_at, :updated_at, :image_file_name, :image_updated_at, :lat, :lng, "post_curses.id", "users.id", "users.fb_id", "users.profile_image_file_name", "users.profile_image_updated_at")
    else
      posts_array = Post.joins(:user)
                  .includes(:user)
                  .where("posts.id > ? and posts.user_id = ?", last_id, user_id)
                  .joins("LEFT OUTER JOIN `post_curses` ON `post_curses`.`post_id` = `posts`.`id` and `post_curses`.`user_id` = #{curse_user_id}")
                  .order(created_at: :asc)
                  .paginate(:page => 1)
                  .pluck_to_hash(:id, :category_id, :curses_count, :street, :city, :state, :reports_count, :description, :created_at, :updated_at, :image_file_name, :image_updated_at, :lat, :lng, "post_curses.id", "users.id", "users.fb_id", "users.profile_image_file_name", "users.profile_image_updated_at")
    end
    if posts_array.nil? then
      nil
    else
      posts = []
      posts_array.each do |obj| 
        posts << map_post(obj)
      end
      
      posts
    end  
  end
  
  def self.select_post(page, curse_user_id, user_id = nil, ios = false)
    if page == 0 then
      page = 1
    end
    
    where_ios = ""
    if ios then
      where_ios = " posts.id >= 79 and posts.id <= 86 "
    end
    
    if user_id.nil? then
      posts_array = Post.joins(:user)
                  .includes(:user)
                  .joins("LEFT OUTER JOIN `post_curses` ON `post_curses`.`post_id` = `posts`.`id` and `post_curses`.`user_id` = #{curse_user_id}")
                  .order(created_at: :desc)
                  .where(where_ios)
                  .paginate(:page => page)
                  .pluck_to_hash(:id, :category_id, :curses_count, :street, :city, :state, :reports_count, :description, :created_at, :updated_at, :image_file_name, :image_updated_at, :lat, :lng, "post_curses.id", "users.id", "users.fb_id", "users.profile_image_file_name", "users.profile_image_updated_at")
    else
      if ios then
        where_ios = " and #{where_ios}"
      end
      posts_array = Post.joins(:user)
                  .includes(:user)
                  .joins("LEFT OUTER JOIN `post_curses` ON `post_curses`.`post_id` = `posts`.`id` and `post_curses`.`user_id` = #{curse_user_id}")
                  .where("posts.user_id = ? #{where_ios}", user_id)
                  .order(created_at: :desc)
                  .paginate(:page => page)
                  .pluck_to_hash(:id, :category_id, :curses_count, :street, :city, :state, :reports_count, :description, :created_at, :updated_at, :image_file_name, :image_updated_at, :lat, :lng, "post_curses.id", "users.id", "users.fb_id", "users.profile_image_file_name", "users.profile_image_updated_at")
   
    end
    if posts_array.nil? then
      nil
    else
      posts = []
      posts_array.each do |obj| 
        posts << map_post(obj)
      end
      
      posts
    end  
  end
  
  def self.select_post_near(page, curse_user_id, lat, lng, ios = false)
    if page == 0 then
      page = 1
    end
    
    where_ios = ""
    if ios then
      where_ios = " posts.id >= 79 and posts.id <= 86 "
    end
    
    posts_array = Post
                      .joins(:user)
                      .includes(:user)
                      .joins("LEFT OUTER JOIN `post_curses` ON `post_curses`.`post_id` = `posts`.`id` and `post_curses`.`user_id` = #{curse_user_id}")
                      .where(where_ios)
                      .having("distance < ?", DISTANCE_FIND)
                      .order("distance")
                      .paginate(:page => page)
                      .pluck_to_hash(:id, "(
                        3959 * acos (cos ( radians(#{lat}) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(#{lng}) )
                        + sin ( radians(#{lat}) )
                        * sin( radians( lat ) )
                      )) AS distance", :category_id, :curses_count, :street, :city, :state, :reports_count, :description, :created_at, :updated_at, :image_file_name, :image_updated_at, :lat, :lng, "post_curses.id", "users.id", "users.fb_id", "users.profile_image_file_name", "users.profile_image_updated_at")

    if posts_array.nil? then
      nil
    else
      posts = []
      posts_array.each do |obj| 
        posts << map_post(obj)
      end
      
      posts
    end  
  end
  
  def image_url
    image.url
  end
  
  private
  
  def self.map_post(obj)
    if obj.nil? then
      nil
    end
    puts obj
    post = Post.new
    post.id = obj[:id]
    post.description = obj[:description]
    post.state = obj[:state]
    post.street = obj[:street]
    post.city = obj[:city]
    post.created_at = obj[:created_at]
    post.updated_at = obj[:updated_at]
    post.curses_count = obj[:curses_count]
    post.reports_count = obj[:reports_count]
    post.image_file_name = obj[:image_file_name]
    post.image_updated_at = obj[:image_updated_at]
    post.category_id = obj[:category_id]
    post.lat = obj[:lat]
    post.lng = obj[:lng]
    
    if obj["post_curses.id"].nil? then
      post.user_curse = false
    else
      post.user_curse = true
    end

    post.build_user(:id => obj["users.id"], :fb_id => obj["users.fb_id"], :profile_image_file_name => obj["users.profile_image_file_name"], :profile_image_updated_at => obj["users.profile_image_updated_at"])

    post
  end
end
