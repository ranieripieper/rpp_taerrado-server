module TaErrado
  module V1
    class SimpleUserSerializer < ActiveModel::Serializer
      attributes :id, :name, :surname, :fullname
    end
  end
end
