# -*- encoding : utf-8 -*-
module TaErrado
  class UserUpdateService < BaseService

    attr_reader :user, :errors, :response_status

    WHITELIST_PARAMETERS = [
      :profile_image,
      :name,
      :email,
      :surname,
      :password,
      :device_token,
      :platform,
      :password_confirmation
    ]
    
    def initialize(user, options={})
      @old_user           = user.dup
      @user               = user
      @options            = options
      @response_status    = 400
    end

    
    def execute
      if valid_user?
        if can_update_profile?
          update_user_attributes
          if @user.save then
            @user            = @user
            @response_status = 200
          else
            @response_status = 400
            add_errors(@user.errors.full_messages)
          end
        else
          @response_status = 403
          add_error(I18n.t('taerrado.errors.users.password_not_match'))
        end
      else
        @response_status = 404
        add_error(I18n.t('taerrado.errors.users.not_found'))
      end
    end

    def update_user_attributes

      user_params.each do |attribute,value|
        @user.send("#{attribute}=", value)
      end

      @user
    end

    def changed_attributes
      @changed_attributes ||= changed_user_attributes

      if @user.valid? && @options[:user] && @options[:user][:password] &&
         @changed_attributes.exclude?(:password)

        @changed_attributes.push(:password)
      end

      @changed_attributes
    end

    def changed_user_attributes
      @user_changed_attributes ||= changes(@old_user, @user, user_params.keys)
    end

    def success?
      valid_user? && @response_status == 200
    end

    def user_params
      params = (@options[:user] || {}).slice(*WHITELIST_PARAMETERS)

      params
    end

    def valid_user?
      @user && @user.is_a?(User) && @user.valid?
    end

    def can_update_profile?
      true
    end

  end
end
