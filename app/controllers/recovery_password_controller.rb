class RecoveryPasswordController < ApplicationController
  
  RESET_PASSWORD_UPDATE_PERIOD = 30.minutes
  
  def index
    token = params[:token]
    flash.clear
    user = nil
    if not token.nil? then
      user = User.where(reset_password_token: params[:token]).first
      if not user.nil? then
        now = Time.zone.now
        if user.reset_password_sended_at.nil? or user.reset_password_sended_at.between?((now - RESET_PASSWORD_UPDATE_PERIOD), now) then
          @user = user
          @user.password = nil
        else
          user.reset_password_sended_at = nil
          user.reset_password_token = nil
          user.save         
        end
      
      end
    end
  end
  
  def update_password
    params[:token] = params[:user][:reset_password_token]
    service  = TaErrado::PasswordRecoveryService.new(params)
    response = service.reset_password
    if service.success?
      
    else
      if service.errors.nil? then
        flash[:notice] = "Ocorreu um erro. Tente novamente."
      else
        flash[:notice] = service.errors
        @user  = User.where(reset_password_token: params[:user][:reset_password_token]).first
        render "index"
      end
       
    end
  end
  
end
