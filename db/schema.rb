# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160510145815) do

  create_table "categories", force: :cascade do |t|
    t.text "name",        limit: 65535, null: false
    t.text "description", limit: 65535
    t.text "icon",        limit: 65535, null: false
  end

  create_table "post_curses", force: :cascade do |t|
    t.integer  "post_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.datetime "deleted_at"
  end

  add_index "post_curses", ["deleted_at"], name: "index_post_curses_on_deleted_at", using: :btree
  add_index "post_curses", ["post_id", "user_id"], name: "index_post_curses_on_post_id_and_user_id", unique: true, using: :btree
  add_index "post_curses", ["post_id"], name: "index_post_curses_on_post_id", using: :btree
  add_index "post_curses", ["user_id"], name: "index_post_curses_on_user_id", using: :btree

  create_table "post_reports", force: :cascade do |t|
    t.integer  "post_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "post_reports", ["post_id", "user_id"], name: "index_post_reports_on_post_id_and_user_id", unique: true, using: :btree
  add_index "post_reports", ["post_id"], name: "index_post_reports_on_post_id", using: :btree
  add_index "post_reports", ["user_id"], name: "index_post_reports_on_user_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.text     "description",        limit: 65535
    t.float    "lat",                limit: 24,                null: false
    t.float    "lng",                limit: 24,                null: false
    t.integer  "curses_count",       limit: 4,     default: 0, null: false
    t.integer  "reports_count",      limit: 4,     default: 0, null: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "state",              limit: 255
    t.string   "city",               limit: 255
    t.string   "street",             limit: 255
    t.datetime "deleted_at"
    t.integer  "category_id",        limit: 4
  end

  add_index "posts", ["category_id"], name: "fk_rails_9b1b26f040", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",                       limit: 255
    t.string   "surname",                    limit: 255
    t.string   "email",                      limit: 255
    t.string   "gender",                     limit: 255
    t.string   "fb_id",                      limit: 255
    t.text     "fb_token",                   limit: 65535
    t.string   "auth_token",                 limit: 255
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.string   "password_digest",            limit: 255
    t.string   "profile_image_file_name",    limit: 255
    t.string   "profile_image_content_type", limit: 255
    t.integer  "profile_image_file_size",    limit: 4
    t.datetime "profile_image_updated_at"
    t.string   "reset_password_token",       limit: 255
    t.datetime "reset_password_sended_at"
    t.datetime "password_reseted_at"
    t.datetime "deleted_at"
    t.boolean  "admin",                                    default: false
    t.boolean  "unlock",                                   default: false
    t.string   "platform",                   limit: 255
    t.string   "device_token",               limit: 255
  end

  add_foreign_key "post_curses", "posts"
  add_foreign_key "post_curses", "users"
  add_foreign_key "posts", "categories"
end
