# -*- encoding : utf-8 -*-
Rails.application.routes.draw do
  mount API::Base => '/'
  root :controller => 'static', :action => '/index.html' 
  
  get 'recuperar_senha' => 'recovery_password#index', as: :recovery_password
  post 'recuperar_senha' => 'recovery_password#update_password', as: :update_password
  
end
